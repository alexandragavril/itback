const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER,varsta VARCHAR(3),cnp VARCHAR(20),sex VARCHAR(2))";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta=req.body.varsta;
    let cnp=req.body.cnp;
    let sex='F';
    let error = [];
    
    if(!nume || !prenume || !telefon || !email || !facebook || !tipAbonament || !nrCard || !cvv || !varsta || !cnp)
      {
        error.push("Unul sau mai multe campuri nu au fost introduse!");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
      }
     else 
      {
        if (nume.length < 3 || nume.length > 20) {
          console.log("Nume invalid!");
          error.push("Nume invalid");
        } else if (!nume.match("^[A-Za-z]+$")) {
          console.log("Numele trebuie sa contina doar litere!");
          error.push("Numele trebuie sa contina doar litere!");
        }
        if (prenume.length < 3 || prenume.length > 20) {
          console.log("Prenume invalid!");
          error.push("Prenume invalid!");
        } else if (!prenume.match("^[A-Za-z]+$")) {
          console.log("Prenumele trebuie sa contina doar litere!");
          error.push("Prenumele trebuie sa contina doar litere!");
        }
        if (telefon.length != 10) {
          console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
          error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
        } else if (!telefon.match("^[0-9]+$")) {
          console.log("Numarul de telefon trebuie sa contina doar cifre!");
          error.push("Numarul de telefon trebuie sa contina doar cifre!");
        }
        if (!email.includes("@gmail.com") && !email.includes("@yahoo.com")) {
          console.log("Email invalid!");
          error.push("Email invalid!");
        }
        if(!facebook.includes("facebook.com")){
            console.log("Link de Facebook invalid!");
            error.push("Link de Facebook invalid!");
        }
        if(!tipAbonament.match("Minim") && !tipAbonament.match("Standard") && !tipAbonament.match("Premium"))
        {
          error.push("Tip abonament gresit!");
          console.log("Tip abonament gresit!");
        }
      
        if(nrCard.length != 16){
            console.log("Numarul de card trebuia sa fie de 16 cifre!");
            error.push("Numarul de card trebuie sa fie de 16 cifre!");
        }
        else if(!nrCard.match("^[0-9]+$"))
        {
            console.log("Numarul de card trebuie sa contina doar cifre!");
            error.push("Numarul de card trebuie sa contina doar cifre!");
        }
        if(cvv.length!=3){
            console.log("Cvv invalid!")
            error.push("Cvv invalid!");
        }
        else if(!cvv.match("^[0-9]+$")){
            console.log("Cvv trebuie sa contina doar cifre!");
            error.push("Cvv trebuie sa contina doar cifre!");
        }
        if(varsta.length<1 || varsta.length>3)
        {
            console.log("Varsta este invalida!");
            error.push("Varsta este invalida!");
        }
        else if(!varsta.match("^[0-9]+$"))
        {
            console.log("Varsta trebuie sa contina doar cifre!");
            error.push("Varsta trebuie sa contina doar cifre!");
        } 
        
        if(cnp.length!=13)
        {
            console.log("CNP-ul trebuie sa contina 13 cifre!");
            error.push("CNP-ul trebuie sa contina 13 cifre!");
        }
        else if(!cnp.match("^[0-9]+$"))
        {
            console.log("CNP-ul trebuie sa contina doar cifre!");
            error.push("CNP-ul trebuie sa contina doar cifre!");
        }
      
        var varsta1=Number(varsta);
        var today=new Date();
        var an=today.getFullYear();
        var luna=today.getMonth()+1;
        var zi=today.getDate();
        var zi1=cnp[5]+cnp[6];
        var luna1=cnp[3]+cnp[4];
        var an1=cnp[1]+cnp[2];
        var w=0;
        if((an-an1)%100 != varsta1) // daca diferenta dintre ani nu coresp cu varsta lui
        {
          if((an-an1)%100-varsta1 != 1) w=1; // daca dif e dif de 1 nu coresp
          else if(luna1<luna) w=1;   //trecem la comparatie pe luni in caz ca dif e 1
               else if(luna1==luna && zi>=zi1) w=1;
        }
        else if(luna1>luna) w=1; 
             else if(luna1==luna && zi1>zi) w=1;

      //  console.log((an-an1)%100-varsta1); 
      //  console.log(luna1);
      //  console.log(zi1);

        if(w==1)
        {
          console.log("Varsta si CNP-ul nu corespund!");
          error.push("Varsta si CNP-ul nu corespund!");
        }

        if(cnp[0] == '1' || cnp[0] == '3' || cnp[0] == '5' ) sex='M';

        connection.query("SELECT * FROM abonament WHERE email = '"+email+"'", function (err, result) {
              if (err) throw err;
              if(result.length)
              {
                console.log("Emailul a mai fost introdus o data!");
                error.push("Emailul a mai fost introdus o data!");
              }
            });
      }

    if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,varsta,cnp,sex) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" +tipAbonament +"','" + nrCard +"','" +cvv+"','"+varsta+"','"+cnp+"','"+sex+"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});